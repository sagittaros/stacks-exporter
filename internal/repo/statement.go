package repo

import (
	"fmt"
	"strings"
)

func pageQueryWhereIDIn(ids []string) string {
	query := `
		SELECT %s FROM stacks
		WHERE id IN (%s)
	`
	fields := strings.Join(new(Document).FieldTags(), ",")
	return fmt.Sprintf(query, fields, whereIDIn(ids))
}

// WhereIDIn generates where condition like `WHERE id IN ("...", "...")`
func whereIDIn(ids []string) string {
	quotedIds := make([]string, len(ids))
	for pos, id := range ids {
		quotedIds[pos] = `'` + id + `'`
	}
	return strings.Join(quotedIds, ",")
}
