package repo

// GetRecords returns a list of []Document{}
func GetRecords(ids []string) (*DocumentBatch, error) {
	documentBatch := DocumentBatch{}
	query := pageQueryWhereIDIn(ids)
	rows, err := Conn.Query(query)
	defer rows.Close()
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		document := Document{}
		fields := document.FieldPointersForScanner()
		err = rows.Scan(fields...)
		if err != nil {
			return nil, err
		}
		documentBatch.Documents = append(documentBatch.Documents, document)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}
	return &documentBatch, nil
}
