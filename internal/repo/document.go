package repo

import (
	"reflect"

	"github.com/lib/pq"
	"gitlab.com/sagittaros/pqnull"
)

// Document is the Solr structure
type Document struct {
	ID          pqnull.NullString   `json:"id"`
	Name        pqnull.NullString   `json:"name"`
	Content     pqnull.NullString   `json:"content"`
	CategoryIDs []pqnull.NullString `json:"category_ids"`
}

// DocumentBatch is a collection of documents
type DocumentBatch struct {
	Documents []Document
}

// FieldTags returns the fields for select statement
func (doc Document) FieldTags() []string {
	var fields []string
	val := reflect.ValueOf(doc)
	for i := 0; i < val.Type().NumField(); i++ {
		fields = append(fields, val.Type().Field(i).Tag.Get("json"))
	}
	return fields
}

// FieldPointersForScanner returns pointers that db.Scan uses
func (doc *Document) FieldPointersForScanner() []interface{} {
	var fields []interface{}
	val := reflect.ValueOf(doc).Elem()
	for i := 0; i < val.Type().NumField(); i++ {
		switch val.Field(i).Interface().(type) {
		case []pqnull.NullString:
			fields = append(fields, pq.Array(val.Field(i).Addr().Interface()))
		default:
			fields = append(fields, val.Field(i).Addr().Interface())
		}
	}
	return fields
}
