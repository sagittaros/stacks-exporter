package storage

import (
	"log"
	"net/url"
	"os"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/aws/external"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/s3/s3manager"
)

type Client struct {
	Uploader   *s3manager.Uploader
	Downloader *s3manager.Downloader
}

func NewS3Client() *Client {
	client := &Client{}

	// We will load AWS config from ENV VAR
	cfg, err := external.LoadDefaultAWSConfig()
	checkErr(err)

	client.Uploader = s3manager.NewUploader(cfg)
	client.Downloader = s3manager.NewDownloader(cfg)
	return client
}

func (client *Client) Download(s3URI, localPath string) {
	bucket, key := s3URIToBucketAndKey(s3URI)

	// Create a file to write the S3 Object contents to.
	f, err := os.Create(localPath)
	if err != nil {
		log.Fatalf("failed to create file %q, %v", localPath, err)
	}

	// // Write the contents of S3 Object to the file
	_, err = client.Downloader.Download(f, &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		log.Fatalf("failed to download file, %v", err)
	}
}

func (client *Client) Upload(localPath, s3URI string) {
	bucket, key := s3URIToBucketAndKey(s3URI)

	f, err := os.Open(localPath)
	if err != nil {
		log.Fatalf("failed to open file %q, %v", localPath, err)
	}

	// Upload the file to S3.
	_, err = client.Uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
		Body:   f,
	})
	if err != nil {
		log.Fatalf("failed to upload file, %v", err)
	}
}

func s3URIToBucketAndKey(s3URI string) (string, string) {
	u, err := url.Parse(s3URI)
	checkErr(err)

	if u.Scheme != "s3" {
		log.Fatalf("Invalid S3 URI format. s3URIToBucketAndKey method does not accept scheme: %s", u.Scheme)
	}

	return u.Host, u.Path[1:]
}

// handleRetryableError. Likely caused by configuration issue
func checkErr(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}
