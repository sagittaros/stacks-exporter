FROM golang:1.10-alpine

RUN apk update
RUN apk add git
RUN go get -u github.com/golang/dep/cmd/dep

WORKDIR /go/src/gitlab.com/sagittaros/stacks-exporter

COPY Gopkg.toml Gopkg.lock ./
RUN dep ensure -vendor-only

COPY . .

RUN go install
ENTRYPOINT ["stacks-exporter"]
