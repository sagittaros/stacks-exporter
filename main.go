package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/sagittaros/pqconn"
	"gitlab.com/sagittaros/stacks-exporter/internal/repo"
	"gitlab.com/sagittaros/stacks-exporter/internal/storage"
)

const (
	docIDKey    = "ml_training_docs"
	docPerBatch = 200
)

type trainingDoc struct {
	ID          string   `json:"id"`
	Name        string   `json:"name"`
	Content     string   `json:"content"`
	LegalTopics []string `json:"legal_topics"`
	DocTypes    []string `json:"doc_types"`
}

func main() {
	dbConnString := flag.String("db", "", "PG connection string")
	redisConnString := flag.String("redis", "redis://127.0.0.1:6379/0", "Redis connection string")
	s3SaveLocation := flag.String("s3", "s3://intelllex-se-datascience/recommender/data", "S3 location to save to")
	flag.Parse()

	repo.Conn = pqconn.EstablishConnection(*dbConnString)
	defer repo.Conn.Close()
	r, _ := redis.DialURL(*redisConnString)
	defer r.Close()

	for {
		ids, _ := redis.Strings(r.Do("SPOP", docIDKey, docPerBatch))
		if len(ids) == 0 {
			break
		}
		batch, err := repo.GetRecords(ids)
		if err != nil {
			fmt.Println(ids)
			fmt.Println("Error: ", err)
			continue
		}
		for _, doc := range batch.Documents {
			tDoc := trainingDoc{ID: doc.ID.String, Name: doc.Name.String}
			tDoc.LegalTopics = make([]string, 0)
			tDoc.DocTypes = make([]string, 0)
			if doc.Content.Valid {
				tDoc.Content = doc.Content.String
			}
			for _, cat := range doc.CategoryIDs {
				catMeta, _ := redis.Strings(r.Do("HGETALL", cat.String))
				if len(catMeta) == 0 {
					continue
				}
				switch catMeta[1] {
				case "legal_topic":
					tDoc.LegalTopics = append(tDoc.LegalTopics, catMeta[3])
				case "doctype":
					tDoc.DocTypes = append(tDoc.DocTypes, catMeta[3])
				}
			}

			storeAsJSON(tDoc, *s3SaveLocation)
		}
	}
}

func storeAsJSON(doc trainingDoc, s3Bucket string) {
	docJSONBytes, _ := json.Marshal(doc)
	ioutil.WriteFile(doc.ID+".json", docJSONBytes, 0644)

	tmpFile, err := ioutil.TempFile(os.TempDir(), "stacksexport-")
	if err != nil {
		log.Fatal("Cannot create temporary file", err)
	}
	defer os.Remove(tmpFile.Name())

	if _, err = tmpFile.Write(docJSONBytes); err != nil {
		log.Fatal("Failed to write to temporary file", err)
	}
	if err := tmpFile.Close(); err != nil {
		log.Fatal(err)
	}

	store := storage.NewS3Client()
	s3Location := filepath.Join(s3Bucket, time.Now().Format("2006-01-02"), doc.ID+".json")
	store.Upload(tmpFile.Name(), s3Location)

}
